Puppet::Parser::Functions.newfunction(:read_password, :arity => 0,
    :type => :rvalue) do |args|
	# maven_password_file = File.expand_path('~/maven_master_password')
	# if File.exist?(maven_password_file)
	# 	pw = File.open(maven_password_file, &:readline)
	# end

    pw = nil
    begin
        pw = Facter.config_maven_master_password || ""
    rescue Exception => e        
    end
    confirm_pw = nil

    if pw == nil
        charlie = AgentCharlie::Charlie.instance
        
        while confirm_pw == nil
        	while pw == nil || pw.length == 0
                pw = charlie.event.raise_event_single(AgentCharlie::Event::PasswordInfoRequest.new("Create a Maven Master Password: ") do |input|
                    input.length > 0
                end)
            end

            confirm_pw = charlie.event.raise_event_single(AgentCharlie::Event::PasswordInfoRequest.new("Confirm Maven Master Password: ") do |input|
                true
            end)
            if confirm_pw != pw
                pw = nil
                confirm_pw = nil
                say("Passwords do not match")
            end
        end
    end

    pw.chomp

end
