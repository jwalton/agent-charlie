# == Class: maven210
#
# Installs Maven210
#

define maven ($version) {
    $repository = $::charlie_repository
    $target = $::config_atlassian_home
    $tmp = $::config_tmp_directory
    $targetdir = "apache-maven-${version}"
    $file = "apache-maven-${version}-bin.zip"
    $user = $::config_user
    $group = $::config_group

    $va = split($version,'[.]')
    $major_version = "${va[0]}"

    file {'tmp_directory':
        path    =>  $tmp,
        ensure  =>  directory,
        owner   =>  $user,
        group   =>  $group,
        recurse =>  inf,
    }

    exec {"maven_get_${version}":
        command => "/usr/bin/curl -L -s -o '${tmp}/${file}' '${repository}/${file}'",
        creates => "${tmp}/${file}",
        require => File['tmp_directory'],
    }

    exec {"maven_unzip_${version}":
        command => "/usr/bin/unzip -nd '${target}' '${tmp}/${file}'",
        # creates => "${target}/apache-maven-${version}",
        require => [ Exec["maven_get_${version}"] ]
    }

    file{"maven_dir_${version}":
        ensure  =>  directory,
        path    =>  "${target}/${targetdir}",
        owner   =>  "${user}",
        group   =>  "${group}",
        mode    =>  "644",
        recurse =>  inf,
        require =>  Exec["maven_unzip_${version}"],
    }

    file{"maven_dir_exec_${version}":
        path    =>  "${target}/${targetdir}/bin/mvn",
        owner   =>  "${user}",
        group   =>  "${group}",
        mode    =>  "755",
        require =>  [ File["maven_dir_${version}"] ]
    }

    if ($version == '2.1.0') {
        file{"maven_agent_executable":
            path    =>  "${target}/bin/mvn",
            owner   =>  $user,
            group   =>  $group,
            mode    =>  "755",
            require =>  File["maven_dir_exec_${version}"],
            content =>  template('maven/maven-binary.erb'),
            recurse =>  inf,
        }
    }

    if ($major_version == '3') {
        file{"maven_agent_executable":
            path    =>  "${target}/bin/mvn3",
            owner   =>  $user,
            group   =>  $group,
            mode    =>  "755",
            require =>  File["maven_dir_exec_${version}"],
            content =>  template('maven/maven-binary.erb'),
            recurse =>  inf,
        }
    }
    
    file{"maven_agent_executable${version}":
        path    =>  "${target}/bin/mvn${version}",
        owner   =>  $user,
        group   =>  $group,
        mode    =>  "755",
        require =>  File["maven_dir_exec_${version}"],
        content =>  template('maven/maven-binary.erb'),
        recurse =>  inf,
    }

    # This is the maven environment file
    file{"maven_environment_${version}":
        path    =>  "${target}/env/mvn${version}-environment",
        ensure  =>  present,
        owner   =>  "${user}",
        group   =>  "${group}",
        content =>  template('maven/maven-environment.erb'),
        recurse =>  inf,
    }

}
