define maven::settings ($version) {

    $group = $::config_group
    $user = $::config_user
    $user_home = $::config_home
    $target = $::config_atlassian_home
    $charlie_repository = $::charlie_repository
    $tmp = $::config_tmp_directory

    $crowd_username = crowd_username()
    $crowd_password = crowd_password()
    $mpasswd = read_password()

    $executable = "${target}/bin/mvn${version}"

    file{'m2_directory_ownership':
        ensure  =>  directory,
        path    =>  "${user_home}/.m2",
        owner   =>  "${user}",
        group   =>  "${group}",
        mode    =>  "0644",
        recurse =>  inf,
    }

    # Checkout the SVN repository into their maven settings directory
    # only if they don't already have maven settings
    exec {'default_maven_settings':
        command =>  "git clone git@bitbucket.org:atlassian/maven-settings.git '${user_home}/.m2'",
        user    =>  $user,
        # command =>  "svn export --non-interactive --trust-server-cert --username '${crowd_username}' --password '${crowd_password}' 'https://svn.atlassian.com/svn/private/atlassian/maven2settings/trunk' '${user_home}/.m2'",
        creates =>  "${user_home}/.m2",
        path    =>  ['/usr/local/bin', '/usr/bin', '/bin'],
        before  =>  File['m2_directory_ownership'],
    }

    # If we don't have a crowd user, we can use these:
    # exec {'download_mvn_settings':
    #     cwd     => "${user_home}",
    #     command => "curl -L -s -o '${tmp}/mvn-settings.zip' '${charlie_repository}/mvn-settings.zip'",
    #     path    => ['/usr/bin','/bin'],
    #     before  => Exec['default_maven_settings'],
    # }
    # exec {'default_maven_settings':
    #     cwd     => "${user_home}",
    #     command => "unzip '${tmp}/mvn-settings.zip'",
    #     creates => "${user_home}/.m2",
    #     path    => ['/usr/bin','/bin'],
    #     before  => File['m2_directory_ownership'],
    # }

    exec {'tmp_mvn_master_password':
        command =>  "${executable} --encrypt-master-password '${mpasswd}' 1> '${user_home}/.m2/tmp_mvn_master_password'",
        creates =>  "${user_home}/.m2/tmp_mvn_master_password",
        unless  =>  "[ -f \"${user_home}/.m2/settings-security.xml\" ]",
        path    =>  ['/usr/bin', '/usr/sbin', '/bin', '/sbin'],
        require =>  [File['m2_directory_ownership']],
    }

    # settings-security.xml
    exec {'settings_security':
        creates =>  "${user_home}/.m2/settings-security.xml",
        path    =>  ['/usr/bin', '/usr/sbin', '/bin', '/sbin'],
        require =>  [File['m2_directory_ownership'],Exec['default_maven_settings'],Exec['tmp_mvn_master_password']],
        before  =>  File['delete_tmp_mvn_master_password'],
        command =>  "cat <<EOF > '${user_home}/.m2/settings-security.xml'
<settingsSecurity>
  <master>\$(cat '${user_home}/.m2/tmp_mvn_master_password')</master>
</settingsSecurity>
EOF
",
    }

    file {'settings_security_ownership':
        path    =>  "${user_home}/.m2/settings-security.xml",
        owner   =>  $user,
        group   =>  $group,
        mode    =>  "0600",
        recurse =>  inf,
        require =>  [File['m2_directory_ownership'], Exec['settings_security']],
    }

    file {'settings_xml_ownership':
        path    =>  "${user_home}/.m2/settings.xml",
        owner   =>  $user,
        group   =>  $group,
        recurse =>  inf,
        require =>  [Exec['sed_settings_xml_replace'], File['m2_directory_ownership']],
    }

    exec {'tmp_mvn_password':
        command =>  "sudo -u '${user}' '${executable}' --encrypt-password '${crowd_password}' 1> '${user_home}/.m2/tmp_password'",
        creates =>  "${user_home}/.m2/tmp_password",
        path    =>  ['/usr/bin', '/usr/sbin', '/bin', '/sbin'],
        require =>  [File['settings_security_ownership']],
    }

    exec {'sed_settings_xml_replace':
        command     =>  "sed -E -e 's#yourusername#${crowd_username}#g;'\"s#yourpassword#\$(cat '${user_home}/.m2/tmp_password')#g\" '${user_home}/.m2/settings.xml.developer' > '${user_home}/.m2/settings.xml'",
        path        =>  ['/usr/bin', '/bin'],
        require     =>  [Exec['default_maven_settings'], Exec['tmp_mvn_password']],
        before      =>  [File['delete_tmp_mvn_password']],
        onlyif      =>  "[ -f \"${user_home}/.m2/tmp_password\" -a -f \"${user_home}/.m2/settings.xml.developer\" ]",
    }

    # exec {'sed_settings_xml_replace_password':
    #     command     =>  "sed -Ei'bak' -e \"s#yourpassword#\$(cat '${user_home}/.m2/tmp_password')#\" '${user_home}/.m2/settings.xml'",
    #     path        =>  ['/usr/bin', '/bin'],
    #     require     =>  [Exec['tmp_mvn_password'],File['symlink_settings']],
    #     before      =>  File['delete_tmp_mvn_password'],
    # }

    file {'delete_tmp_mvn_password':
        path        =>  "${user_home}/.m2/tmp_password",
        ensure      =>  absent,
    }

    file {'delete_tmp_mvn_master_password':
        path        =>  "${user_home}/.m2/tmp_mvn_master_password",
        ensure      =>  absent,
    }
}
