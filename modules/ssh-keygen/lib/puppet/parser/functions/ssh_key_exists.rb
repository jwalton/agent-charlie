
Puppet::Parser::Functions.newfunction(:ssh_key_exists, :arity => 0,
    :type => :rvalue) do |args|
    File.exists?(File.expand_path('~') + '/.ssh/id_rsa')
end
