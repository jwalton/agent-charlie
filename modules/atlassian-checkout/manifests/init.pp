class atlassian-checkout {
    $tmp = $::config_tmp_directory
    $target = $::config_atlassian_home
    $user_home = $::config_home
    $user = $::config_user
    $group = $::config_group
    $repository = $::charlie_repository

    # First we'll download atlassian-checkout
    exec {'download_atlassian_checkout':
        cwd     => "${target}",
        command => "git clone git@bitbucket.org:robertmassaioli/atlassian-checkout.git",
        path    => ['/usr/local/bin', '/usr/bin','/bin'],
        creates => "${target}/atlassian-checkout",
        user    => "${user}"
    }

    file {'atlassian_checkout_ownership':
        path    => "${target}/atlassian-checkout",
        owner   => $user,
        group   => $group,
        recurse => inf,
        require => Exec['download_atlassian_checkout'],
    }

    # Set up the environment for atlassian-checkout
    file{"atlassian_checkout_environment":
        path    =>  "${target}/env/atlassian-checkout-environment",
        ensure  =>  present,
        owner   =>  "${user}",
        group   =>  "${group}",
        content =>  template('atlassian-checkout/atlassian-checkout-environment.erb'),
        recurse =>  inf,
        require =>  Exec['download_atlassian_checkout'],
    }

    file {"atlassian_checkout_executable":
        ensure  =>  link,
        path    =>  "${target}/bin/atlassian-checkout",
        target  =>  "${target}/atlassian-checkout/atlassian-checkout",
        owner   =>  $user,
        group   =>  $group,
        mode    =>  "755",
    }

    # file{"atlassian_checkout_executable":
    #     path    =>  "${target}/bin/atlassian-checkout",
    #     owner   =>  $user,
    #     group   =>  $group,
    #     mode    =>  "755",
    #     content =>  template('atlassian-checkout/atlassian-checkout-binary.erb'),
    #     recurse =>  inf,
    #     require =>  Exec['download_atlassian_checkout'],
    # }

    # Run atlassian-checkout init
    exec{'atlassian_checkout_init':
        cwd     =>  "${target}/atlassian-checkout",
        command =>  "'${target}/bin/atlassian-checkout' init",
        require =>  File['atlassian_checkout_executable'],
        user    =>  $user,
        environment => "HOME=${user_home}",
    }

}
