class hipchat::darwin {
    # Download from repository
    $tmp = $::config_tmp_directory
    $repository = $::charlie_repository
    $target = $::config_atlassian_home

    $home = $::config_home
    $user = $::config_user
    $group = $::config_group

    exec {'download_dmg':
        command => "/usr/bin/curl -L -s -o '${tmp}/HipChat_latest_osx.zip' '${repository}/HipChat_latest_osx.zip'",
        creates => "${tmp}/HipChat_latest_osx.zip",
    }

    exec{'unzip_hipchat':
        command => "/usr/bin/unzip -d '/Applications' '${tmp}/HipChat_latest_osx.zip'",
        creates =>  "/Applications/HipChat.app",
        require => Exec['download_dmg'],
    }

    file{'hipchat_permissions':
        path    => "/Applications/HipChat.app",
        owner   => $user,
        group   => $group,
        recurse => inf,
        require => Exec['unzip_hipchat']
    }

    file{'remove_hipchat_tmp':
        path    => "${tmp}/HipChat_latest_osx.zip",
        ensure  => absent,
        require => Exec['unzip_hipchat'],
    }
}