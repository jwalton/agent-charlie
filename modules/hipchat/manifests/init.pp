class hipchat {
    anchor { 'hipchat::begin': }

    case $::operatingsystem {
        Darwin: {
            class {'hipchat::darwin':
                require => Anchor['hipchat::begin'],
                before  => Anchor['hipchat::end'],
            }
        }
        Ubuntu: {
            class {'hipchat::ubuntu':
                require => Anchor['hipchat::begin'],
                before  => Anchor['hipchat::end'],
            }
        }
        default: {
            fail("Please install HipChat manually for ${::operatingsystem}")
        }
    }

    anchor { 'hipchat::end': }
}