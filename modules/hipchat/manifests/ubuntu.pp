class hipchat::ubuntu {
    exec {'atlassian-repository':
        command => "echo \"deb http://downloads.hipchat.com/linux/apt stable main\" > /etc/apt/sources.list.d/atlassian-hipchat.list",
        path    => ['/usr/local/bin','/usr/local/sbin','/usr/bin','/usr/sbin','/bin','/sbin'],
        creates => "/etc/apt/sources.list.d/atlassian-hipchat.list",
        notify  => Exec['update-apt-repo'],
    }

    exec {'atlassian-public-key':
        command => 'wget -O - https://www.hipchat.com/keys/hipchat-linux.key | apt-key add -',
        path    => ['/usr/local/bin','/usr/local/sbin','/usr/bin','/usr/sbin','/bin','/sbin'],
        require => Exec['atlassian-repository'],
    }

    package {'hipchat':
        ensure  => installed,
        require => [Exec['atlassian-repository'],Exec['atlassian-public-key'],Exec['update-apt-repo']],
    }

    exec {'update-apt-repo':
        command => "apt-get update",
        path    => ['/usr/local/bin','/usr/local/sbin','/usr/bin','/usr/sbin','/bin','/sbin'],
    }
}