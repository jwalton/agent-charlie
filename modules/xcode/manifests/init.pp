# == Class: xcode
#
# Sets up XCode for OSX
#

class xcode {

    if $::operatingsystem != 'Darwin' {
        fail ('XCode Command Lines tools supported on OS X only')
    }

    case $::macosx_productversion_major {
        /10\.9/: {
            $osname = 'mavericks'
        }
        /10\.8/: {
            $osname = 'mountainlion'
        }
        /10\.7/: {
            $osname = 'lion'
        }
        default: {
            fail('OS X version is not supported')
        }
    }

    $repository = $::charlie_repository

    package {'xcode_command_line_tools':
        ensure      =>  installed,
        provider    =>  pkgdmg,
        source      =>  "${repository}/xcode_latest_${osname}.dmg",
    }

}
