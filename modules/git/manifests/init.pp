# == Class: git
#
# Installs git
class git {
    $tmp = $::config_tmp_directory
    $charlie_dir = $::config_charlie_home
    $repository = $::charlie_repository

    $home = $::config_home
    $user = $::config_user
    $group = $::config_group

    $crowd_username = crowd_username()
    $full_name = $::full_name

    case $::operatingsystem {
        ubuntu: {
            package { "git":
                ensure => installed,
                before => Exec['git_config_name']
            }
        }
        Darwin: {
            exec {'install_git':
                command => "${charlie_dir}/bin/brew-bottle git",
                creates => "/usr/local/Cellar/git",
                before => Exec['git_config_name']
            }
        }
        default: {
            fail("Unsupported operating: $::operatingsystem")
        }
    }

    # Configure the user's name and stuff
    exec {'git_config_name':
        command =>  "git config --global user.name \"${full_name}\"",
        user    =>  $user,
        environment     =>  "HOME=${home}",
        path    =>  ['/usr/local/bin', '/usr/bin']
    }

    exec {'git_config_email':
        command =>  "git config --global user.email '${crowd_username}@atlassian.com'",
        user    =>  $user,
        environment     =>  "HOME=${home}",
        path    =>  ['/usr/local/bin', '/usr/bin']
    }
}
