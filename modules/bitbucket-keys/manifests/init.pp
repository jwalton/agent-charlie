class bitbucket-keys {
    $bitbucket_username = bitbucket_username()
    $bitbucket_password = bitbucket_password()

    $user_home = $::config_home
    $user = $::config_user
    $group = $::config_group
    $charlie_dir = $::config_charlie_home

    # $success = bitbucket_ssh_key($bitbucket_username, $bitbucket_password)
    # if $success == 'false' {
    #     fail("Could not upload key to bitbucket")
    # }

    exec{'verify_bitbucket_host_key':
        command => "ssh -Tq -o StrictHostKeyChecking=no git@bitbucket.org",
        user    => "${user}",
        returns => [ 0, 255 ],
        path    => ['/usr/local/bin', '/usr/bin','/bin']
    }

    upload_bitbucket_keys {'upload_keys':
        home     => $user_home,
        username => $bitbucket_username,
        password => $bitbucket_password
    }
}
