# == Class: jdk::darwin
#
# Sets up JDK 7 for OS X
#

class jdk::darwin {
    # For instance this will be 7u17
    $user = $::config_user
    $group = $::config_group
    $repository = $::charlie_repository
    $target = $::config_atlassian_home

    package {'jdk':
        ensure => installed,
        source => "${repository}/jdk_latest.dmg"
    }

    # JDK environment file
    file{"jdk_environment":
        path    =>  "${target}/env/jdk-environment",
        ensure  =>  present,
        owner   =>  $user,
        group   =>  $group,
        content =>  "export JAVA_HOME=`/usr/libexec/java_home`",
        recurse =>  inf,
    }
}
