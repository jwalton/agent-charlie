class stash-keys {
    $crowd_username = crowd_username()
    $crowd_password = crowd_password()

    $user_home = $::config_home
    $user = $::config_user
    $group = $::config_group
    $charlie_dir = $::config_charlie_home

    # Upload your stash keys
    upload_stash_keys{'upload_stash_keys':
        home           => $user_home,
        stash_username => $crowd_username,
        stash_password => $crowd_password
    }

}