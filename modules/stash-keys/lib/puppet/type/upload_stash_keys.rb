
require 'rest-client'

Puppet::Type.newtype(:upload_stash_keys) do
    @doc = "Uploads your private key to Stash"

    newproperty(:stupid_hack) do |property|
        desc "Stupid hack to get puppet to do what we want"

        defaultto :insync

        def retrieve
            :outofsync
        end

        def sync
            sshkey = File.read("#{self.resource[:home]}/.ssh/id_rsa.pub").chomp
            jsonRequest = "{\"text\":\"#{sshkey}\"}"

            begin
                response = RestClient::Request.execute(
                    :method     =>  :post,
                    :url        =>  'https://stash.atlassian.com/rest/ssh/1.0/keys',
                    :payload    =>  jsonRequest,
                    :headers    =>  {
                        :content_type   =>  :json,
                        :accept         =>  :json
                    },
                    :user       =>  self.resource[:stash_username],
                    :password   =>  self.resource[:stash_password]
                )
            rescue RestClient::Conflict
            rescue => e
                self.fail "Could upload SSH keys: #{e.message}"
            end

            :insync
        end
    end

    newparam(:name) do
        desc "The name of the resource"
    end

    newparam(:home) do
        desc "The home directory of the user"
    end

    newparam(:stash_username) do
        desc "Your stash username"
    end

    newparam(:stash_password) do
        desc "Your stash password"
    end

end
