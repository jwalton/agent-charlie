# == Class: python
#
# Installs Python3
class python3 {
    $tmp = $::config_tmp_directory
    $charlie_dir = $::config_charlie_home
    $repository = $::charlie_repository

    $user = $::config_user
    $group = $::config_group

    case $::operatingsystem {
        ubuntu: {
            package { "python3": ensure => installed }
        }
        Darwin: {
            exec {'install_python3':
                command => "${charlie_dir}/bin/brew-bottle python3",
                creates => "/usr/local/Cellar/python3",
            }
        }
        default: {
            fail("Unsupported operating: $::operatingsystem")
        }
    }
}
