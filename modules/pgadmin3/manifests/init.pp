class pgadmin3 {
    anchor { 'pgadmin3::begin': }

    case $::operatingsystem {
        Darwin: {
            class {'pgadmin3::darwin':
                require => Anchor['pgadmin3::begin'],
                before  => Anchor['pgadmin3::end'],
            }
        }
        Ubuntu: {
            class {'pgadmin3::ubuntu':
                require => Anchor['pgadmin3::begin'],
                before  => Anchor['pgadmin3::end'],
            }
        }
        default: {
            fail("Unsupported Operating System for pgadmin3: ${::operatingsystem}")
        }
    }

    anchor { 'pgadmin3::end': }
}