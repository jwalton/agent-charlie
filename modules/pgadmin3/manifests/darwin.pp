class pgadmin3::darwin {
    $tmp = $::config_tmp_directory
    $repository = $::charlie_repository

    $home = $::config_home
    $user = $::config_user
    $group = $::config_group

    exec {'download_dmg':
        command => "/usr/bin/curl -L -s -o '${tmp}/pgadmin3_latest.dmg' '${repository}/pgadmin3_latest.dmg'",
        creates => "${tmp}/pgadmin3_latest.dmg",
    }

    exec {'convert_dmg':
        command => "/usr/bin/hdiutil convert -quiet ${tmp}/pgadmin3_latest.dmg -format UDTO -o ${tmp}/pgadmin3_latest",
        creates => "${tmp}/pgadmin3_latest.cdr",
        require => Exec['download_dmg'],
    }

    exec {'mount_dmg':
        command => "/usr/bin/hdiutil attach -quiet -nobrowse -noverify -noautoopen -mountpoint ${tmp}/pgadmin3_latest_mount ${tmp}/pgadmin3_latest.cdr",
        creates => "${tmp}/pgadmin3_latest_mount",
        require => Exec['convert_dmg'],
    }

    exec {'copy_app':
        command => "/bin/cp -R ${tmp}/pgadmin3_latest_mount/pgAdmin3.app /Applications",
        creates => "/Applications/pgAdmin3.app",
        require => Exec['mount_dmg'],
    }

    file {'pg3_application':
        path    => "/Applications/pgAdmin3.app",
        owner   => $user,
        group   => $group,
        recurse => inf,
        require => Exec['copy_app'],
    }

    exec {'unmount_dmg':
        command => "/usr/bin/hdiutil detach ${tmp}/pgadmin3_latest_mount",
        onlyif  => "/bin/[ -e \"${tmp}/pgadmin3_latest_mount\" ]",
        require => Exec['copy_app'],
    }

    file {'clean_dmg':
        path    => "${tmp}/pgadmin3_latest.dmg",
        ensure  => absent,
        require => Exec['convert_dmg'],
        recurse => inf,
    }

    file {'clean_converted':
        path    => "${tmp}/pgadmin3_latest.cdr",
        ensure  => absent,
        require => Exec['unmount_dmg'],
        recurse => inf,
    }
}