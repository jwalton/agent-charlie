class pgadmin3::ubuntu {
    package{'pgadmin3':
        ensure  => 'installed'
    }
}