class homebrew::install {
  $directories = [ '/usr/local',
                   '/usr/local/bin',
                   '/usr/local/Cellar',
                   '/usr/local/etc',
                   '/usr/local/include',
                   '/usr/local/lib',
                   '/usr/local/lib/pkgconfig',
                   '/usr/local/Library',
                   '/usr/local/sbin',
                   '/usr/local/share',
                   '/usr/local/var',
                   '/usr/local/var/log',
                   '/usr/local/share/locale',
                   '/usr/local/share/man',
                   '/usr/local/share/man/man1',
                   '/usr/local/share/man/man2',
                   '/usr/local/share/man/man3',
                   '/usr/local/share/man/man4',
                   '/usr/local/share/man/man5',
                   '/usr/local/share/man/man6',
                   '/usr/local/share/man/man7',
                   '/usr/local/share/man/man8',
                   '/usr/local/share/info',
                   '/usr/local/share/doc',
                   '/usr/local/share/aclocal' ]

  $user = $::config_user
  $group = $::config_group
  $user_home = $::config_home
  $tmp = $::config_tmp_directory
  $charlie_dir = $::config_charlie_home
  $target = $::config_atlassian_home
  $repository = $::charlie_repository

  case $::macosx_productversion_major {
      /10\.9/: {
          $osx_version = 'mavericks'
      }
      /10\.8/: {
          $osx_version = 'mountain_lion'
      }
      /10\.7/: {
          $osx_version = 'lion'
      }
      default: {
          fail('OS X version is not supported')
      }
  }

  file { $directories:
    ensure   => directory,
    owner    => $homebrew::user,
    group    => 'admin',
    mode     => 0775,
    recurse  => inf,
  }

  exec { 'install-homebrew':
    cwd       => '/usr/local',
    command   => "/usr/bin/su ${homebrew::user} -c '/bin/bash -o pipefail -c \"/usr/bin/curl -skSfL https://github.com/mxcl/homebrew/tarball/master | /usr/bin/tar xz -m --strip 1\"'",
    creates   => '/usr/local/bin/brew',
    logoutput => on_failure,
    timeout   => 0,
    require   => File[$directories],
  }

  file { '/usr/local/bin/brew':
    owner     => $homebrew::user,
    group     => 'admin',
    mode      => 0775,
    require   => Exec['install-homebrew'],
    recurse   => inf,
  }

  ##################################################
  # THESE TWO BINARIES ARE AGENT-CHARLIE SPECIFIC  #
  # That is why they go into the agent-charlie bin #
  ##################################################
  file{'brew_binary':
    path    =>  "${charlie_dir}/bin/brew",
    owner   =>  $user,
    group   =>  $group,
    mode    =>  "755",
    content =>  template('homebrew/brew-binary.erb'),
    recurse =>  inf,
    require =>  File['/usr/local/bin/brew'],
  }

  file{'brew-bottle_binary':
    path    =>  "${charlie_dir}/bin/brew-bottle",
    owner   =>  $user,
    group   =>  $group,
    mode    =>  "755",
    content =>  template('homebrew/brew-bottle-binary.erb'),
    recurse =>  inf,
    require =>  File['/usr/local/bin/brew'],
  }
  ##################################################

  file {'brew_environment':
    path    =>  "${target}/env/brew-environment",
    ensure  =>  present,
    owner   =>  $user,
    group   =>  $group,
    content =>  template('homebrew/brew-environment.erb'),
    recurse =>  inf,
  }
}
