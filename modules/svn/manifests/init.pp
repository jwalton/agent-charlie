# == Class: subversion
#
# Installs subversion
class svn {
    $tmp = $::config_tmp_directory
    $charlie_dir = $::config_charlie_home
    $repository = $::charlie_repository

    $user = $::config_user
    $group = $::config_group

    case $::operatingsystem {
        ubuntu: {
            package { "subversion": ensure => installed }
        }
        Darwin: {
            exec {'install_subversion':
                command => "${charlie_dir}/bin/brew-bottle subversion",
                creates => "/usr/local/Cellar/subversion",
            }
        }
        default: {
            fail("Unsupported operating: $::operatingsystem")
        }
    }
}
