# == Class: intellij::darwin
#
# Sets up intellij 12 for OSX
#

class idea::intellij::darwin () {
    # Darwin is easy
	$repository = $::charlie_repository
    $user = $::config_user
    $group = $::config_group
    $user_home = $::config_home

	$config_home = "${user_home}/Library/Preferences/IntelliJIdea12"
    $key_file = "idea12.key"

    $remote_file = "$repository/ideaIU12_latest.dmg"

    package {"intellij_idea":
        ensure   => installed,
        source   => "${remote_file}",
        provider => 'appdmg',
    }

    # Set up IntelliJ settings
    file {"intellij_settings_dir":
        ensure  =>  directory,
        path    =>  "${config_home}",
        owner   =>  $user,
        group   =>  $group,
        require =>  Package["intellij_idea"],
        recurse =>  inf,
    }

    exec {"intellij_license":
        command =>  "/usr/bin/curl -L -s -o '${config_home}/${key_file}' '${repository}/idea12.key'",
        require =>  File["intellij_settings_dir"],
        creates =>  "${config_home}/${key_file}",
        before  =>  File["intellij_license_ownership"],
    }

    file {"intellij_license_ownership":
        path    =>  "${config_home}/${key_file}",
        owner   =>  $user,
        group   =>  $group,
        require =>  Exec["intellij_license"],
        recurse =>  inf,
    }

    file {"intellij_config":
        ensure  =>  present,
        path    =>  "${config_home}/idea.vmoptions",
        require =>  File["intellij_settings_dir"],
        content =>  template('idea/vmoptions.erb'),
        owner   =>  $user,
        group   =>  $group,
        recurse =>  inf,
    }

    file{'intellij_permissions':
        path    => "/Applications/IntelliJ IDEA 12.app",
        owner   => $user,
        group   => $group,
        recurse => inf,
        require => Package['intellij_idea']
    }
}
