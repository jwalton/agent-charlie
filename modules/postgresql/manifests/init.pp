class postgresql {
    anchor { 'postgresql::begin': }

    case $::operatingsystem {
        Darwin: {
            class {'postgresql::darwin':
                require => Anchor['postgresql::begin'],
                before  => Anchor['postgresql::end'],
            }
        }
        Ubuntu: {
            class {'postgresql::ubuntu':
                require => Anchor['postgresql::begin'],
                before  => Anchor['postgresql::end'],
            }
        }
        default: {
            fail("Unsupported Operating System for PostgreSQL: ${::operatingsystem}")
        }
    }

    anchor { 'postgresql::end': }
}