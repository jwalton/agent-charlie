class atlassian-scripts {
    $tmp = $::config_tmp_directory
    $target = $::config_atlassian_home
    $user_home = $::config_home
    $user = $::config_user
    $group = $::config_group
    $repository = $::charlie_repository

    $crowd_username = crowd_username()
    $crowd_password = crowd_password()

    # First we'll download atlassian-scripts
    # exec {'download_atlassian_scripts':
    #     command => "curl -o '${tmp}/atlassian-scripts.tar.gz' '${repository}/sourcecode/atlassian-scripts.tar.gz'",
    #     creates => "${tmp}/atlassian-scripts.tar.gz",
    #     path    => ['/bin','/usr/bin'],
    # }

    # exec {'get_atlassian_scripts':
    #     cwd     => "${target}",
    #     command => "tar xf '${tmp}/atlassian-scripts.tar.gz'",
    #     path    => ['/usr/bin','/bin'],
    #     creates => "${target}/atlassian-scripts",
    #     require => Exec['download_atlassian_scripts'],
    # }

    # We have an EAC test user now, so we can do this
    exec {'get_atlassian_scripts':
       command =>  "svn checkout --non-interactive --trust-server-cert --username '${crowd_username}' --password '${crowd_password}' 'https://svn.atlassian.com/svn/private/atlassian/atlassian-scripts/trunk' '${target}/atlassian-scripts'",
       creates =>  "${target}/atlassian-scripts",
       path    =>  ['/usr/bin', '/bin'],
    }

    file {'atlassian_scripts_ownership':
        path    => "${target}/atlassian-scripts",
        owner   => $user,
        group   => $group,
        recurse => inf,
        require => Exec['get_atlassian_scripts'],
    }

    # Set up the environment for atlassian-scripts
    file{"atlassian_scripts_environment":
        path    =>  "${target}/env/atlassian-scripts-environment",
        ensure  =>  present,
        owner   =>  "${user}",
        group   =>  "${group}",
        content =>  template('atlassian-scripts/atlassian-scripts-environment.erb'),
        recurse =>  inf,
        require =>  Exec['get_atlassian_scripts'],
    }
}
