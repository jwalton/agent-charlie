
module DropPermissions

    def DropPermissions::dropRoot(&block)
        whoami = `who am i | cut -f1 -d' '`.chomp
        my_id = `id -u #{whoami}`.chomp.to_i

        current_id = Process::Sys::geteuid

        begin
            Process::Sys::seteuid(my_id)
            return block.call
        ensure
            Process::Sys::seteuid(current_id)
        end
    end

end