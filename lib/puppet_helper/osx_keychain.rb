require 'puppet_helper/charlie_keyring'

if not Object.const_defined?('OSXKeychain')
    class OSXKeychain < CharlieKeyring
        def initialize(displayName, name)
            @displayName = displayName
            @name = name
        end

        def storePassword(username, password)
            # Remove all -s crowd keychain items
            begin
                %x( security delete-generic-password -s agent-charlie-#{@name} &> /dev/null )
            end while $?.exitstatus == 0

            %x( security add-generic-password -a "#{username}" -s agent-charlie-#{@name} -w "#{password}" -U &> /dev/null )

            return true
        end

        def refreshFromKeyring()
            # Try and read the crowd username and password from the keychain
            keychainPassword = %x( security find-generic-password -s agent-charlie-#{@name} -gw 2> /dev/null )
            if $?.exitstatus == 0
                keychainUsername = %x( security find-generic-password -s agent-charlie-#{@name} | grep -Ee "^[ ]+\\"acct\\"" | cut -f 4 -d'"' 2> /dev/null )

                @username = keychainUsername.chomp
                @password = keychainPassword.chomp
                return true
            else
                return false
            end
        end

        def retrieveUsername()
            @username
        end

        def retrievePassword()
            @password
        end
    end
end