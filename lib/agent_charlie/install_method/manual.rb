class AgentCharlie::InstallMethod::Manual < AgentCharlie::InstallMethod::Base

    def self.description
        "Installed using a custom installation method"
    end

    def parse(data)
        @description = data["manual-desc"] || ''
    end

    def description
        return @description
    end

end

AgentCharlie::InstallMethod.new_method(AgentCharlie::InstallMethod::Manual, "manual")