
class AgentCharlie::Action::List

    def initialize(manifests)
        @manifests = manifests
        AgentCharlie::Logger.debug{"Initializing list action with manifests '#{@manifests.inspect}'"}
    end

    def run_action(progress)
        @progress = progress
        charlie = AgentCharlie::Charlie.instance

        if @manifests.length == 0
            # Print all manifests
            progress.max = 1

            manifests = charlie.manifest_collection.get_manifests
            progress.increment("Found manifests")

            progress.complete("All manifests found", manifests)
        else
            # Print those specific manifests (for the ones that exist)
            progress.max = @manifests.length

            manifests = []
            @manifests.each do |manifest|
                found = charlie.manifest_collection.get_manifest(manifest)
                if found != nil
                    manifests << found
                    progress.increment(manifest)
                else
                    progress.failed("#{manifest} was not found")
                    return
                end
            end
            progress.complete("All manifests found", manifests)
        end
    end

    def get_progress
        return @progress
    end

end