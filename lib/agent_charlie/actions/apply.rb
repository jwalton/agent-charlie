
require 'puppet/util/command_line'

class AgentCharlie::Action::Apply

    def initialize(params = {})
        @reinstall = params[:reinstall] || false
        @manifests = params[:manifests]

        @progress = nil

        AgentCharlie::Logger.debug{"Apply action created"}
    end

    def run_action(progress)
        @progress = progress

        manifest_keys = @manifests.map{ |m| m.key }

        # If we selected reinstall then just delete metadata
        if @reinstall
            AgentCharlie::Logger.debug{"Deleting metadata for provided manifests"}
            File.delete(*manifest_keys.map do |manifest|
                File.join(AgentCharlie.charlie_home, "metadata", manifest)
            end.find_all do |fname|
                AgentCharlie::Logger.debug{"Deleting metadata '#{fname}' (if exists)"}
                File.file?(fname)
            end)
        end

        # Run puppet setup
        wd = Dir.getwd
        Dir.chdir(AgentCharlie.charlie_root)

        custom_args = [
            '--libdir', 'lib',
            '--modulepath', 'modules',
            '--manifestdir', 'manifests',
            '--factpath', 'lib/facts',
        ]
        custom_args += manifest_keys

        # Launch puppet
        Puppet::Util::CommandLine.new('compile', custom_args).execute

        Dir.chdir(wd)
    end

    def get_progress
        return @progress
    end

end