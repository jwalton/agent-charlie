require 'highline/import'
require 'trollop'

class AgentCharlie::CommandLine

    class Base
        attr_accessor :command_name
        attr_accessor :command_description

        def initialize(name, description)
            @command_name = name
            @command_description = description
            @unattended = false
        end

        def execute(args)
            raise 'Execute method has not been implemented'
        end
    end

    @@command_list = []

    def self.newCommand(command)
        @@command_list << command
    end

    def initialize(args)
        @args = args.clone

        @commands = []
        @@command_list.each do |cls|
            @commands << cls.new
        end
    end

    def handle_string_input_request(event)
        return event.default if @unattended

        begin
            result = ask(event.prompt)
        end until event.verify.call(result)

        return result
    end

    def handle_password_input_request(event)
        return event.default if @unattended

        begin
            result = ask(event.prompt) { |question|
                question.echo = '*'
            }
        end until event.verify.call(result)

        return result
    end

    def handle_boolean_input_request(event)
        return event.default if @unattended
        
        begin
            result = agree(event.prompt)
        end until event.verify.call(result)

        return result
    end

    def run_threaded(charlie)
        evt = charlie.event
        evt.register_listener(AgentCharlie::Event::StringInfoRequest, &method(:handle_string_input_request))
        evt.register_listener(AgentCharlie::Event::PasswordInfoRequest, &method(:handle_password_input_request))
        evt.register_listener(AgentCharlie::Event::BooleanInfoRequest, &method(:handle_boolean_input_request))
        Thread.new {
            AgentCharlie::Logger.debug { 'Starting Command Line thread' }
            parse_args
        }
    end

    def sub_commands
        @commands.map do |command|
            command.command_name
        end
    end

    def formatted_command_help
        @commands.map do |command|
            '%15s:   %s' % [command.command_name, command.command_description]
        end
    end

    def sudo(prompt, command, *args)
        puts <<-'EOS'.gsub(/^\t*/, '')

			Agent Charlie requires root access to continue and will prompt you for your password.

		EOS
        Process.exec('sudo', '-E', '-p', prompt, *args)
    end

    def parse_args

        # First thing to do is check if we have root
        if Process::Sys.geteuid != 0
            puts <<-'EOS'.gsub(/^\t*/, '')
				    _                    _      ____ _                _ _
				   / \   __ _  ___ _ __ | |_   / ___| |__   __ _ _ __| (_) ___
				  / _ \ / _` |/ _ \ '_ \| __| | |   | '_ \ / _` | '__| | |/ _ \
				 / ___ \ (_| |  __/ | | | |_  | |___| | | | (_| | |  | | |  __/
				/_/   \_\__, |\___|_| |_|\__|  \____|_| |_|\__,_|_|  |_|_|\___|
				        |___/
			EOS
        end

        sc = sub_commands
        ch = formatted_command_help

        charlie = AgentCharlie::Charlie.instance

        # Parse Agent Charlie global options
        global_opts = Trollop::options(@args) do
            version 'Agent Charlie (c) 2013 Atlassian'
            banner <<-EOS.gsub(/^\t*/, '')
				Agent Charlie is a tool that installs things for you.

				Usage:
				   #{$0} [options] <command> [suboptions]

				Options:
			EOS
            opt :verbose, 'Verbose output from Agent Charlie', :default => false, :short => '-v'
            opt :unattended, 'Ignore standard input and get values from configuration files', :default => false, :short => '-u'

            stop_on sc

            command_help = ch.join("\n")
            banner <<-EOS.gsub(/^\t*/, '')

				Commands:
				#{command_help}

				More:
			EOS
        end

        # Process the options
        AgentCharlie::Logger.turn_on_debug if global_opts[:verbose]
        @unattended = global_opts[:unattended]

        # Process everything else
        subcommand = @args.shift

        if !subcommand
            Trollop::die 'Missing command'
        end

        @commands.each do |command|
            if command.command_name == subcommand
                AgentCharlie::Logger.debug { "Running command '#{subcommand}'" }
                command.execute(@args)
                return
            end
        end

        Trollop::die "Could not find subcommand: '#{subcommand}'"
    end


    def self.generate_doc(list)
        method_hash = {}

        # Categorize the manifests based on the method of installation
        list.each do |manifest|
            method = manifest.method

            method_hash[method.class] ||= []
            method_hash[method.class] << manifest
        end

        docco = ''
        # Return the docco
        method_hash.each_key do |key|
            docco << key.description << "\n"
            method_hash[key].each do |manifest|
                docco << '   %-32s %s' % [manifest.name, manifest.method.description] << "\n"
            end
            docco << "\n"
        end

        return docco if docco.length > 0
        return ' - None'
    end
end

# Include all command line modules
Dir[File.join(File.expand_path('lib/agent_charlie/command_line', AgentCharlie.charlie_root), '*.rb')].each do |file|
    require 'agent_charlie/command_line/' + File.basename(file, File.extname(file))
end
