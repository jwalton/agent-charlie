Agent Charlie
=

Install:
-

For information about how to bootstrap Agent Charlie on your computer, please see:

`https://extranet.atlassian.com/display/DRAKE/Install`

Running:
-

The installation process will install any system wide tools needed for charlie to execute, and install itself in to `$HOME/atlassian/agent-charlie`. 

Use `charlie help setup` to start the provisioning process.

Issues:
-

If you find issues with charlie or its bootstrap process please raise bugs in the DRAKE project on sdog.jira.com (`https://sdog.jira.com/browse/DRAKE`).

