# name: Homebrew
# description: OS X Package Manager
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         manual
#       manual-desc:    Installed via official homebrew installer
#     dependencies:
#       - xcode

if $::operatingsystem == "Darwin" {
	# import "xcode.pp"

	class {'homebrew':
		user 	=>	$::config_user,
	}

	# Class['homebrew'] -> Class['xcode']
} else {
    notify {"Skipping Homebrew - not OSX!":}
}