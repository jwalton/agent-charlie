# name: pgAdmin3
# description: PostgreSQL Administration Interface
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         appdmg
#     dependencies:
#       - postgresql
#   Ubuntu:
#     install-method:
#       method:         apt
#       apt-repository: canonical
#       apt-package:    pgadmin3
#     dependencies:
#       - postgresql

include pgadmin3