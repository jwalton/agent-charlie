# name: SSH Key Generation
# description: Generates SSH keys if you don't already have an id_rsa in your .ssh directory
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         manual
#       manual-desc:    SSH keys generated with ssh-keygen if keys do not already exist
#     dependencies: []
#   Ubuntu:
#     install-method:
#       method:         manual
#       manual-desc:    SSH keys generated with ssh-keygen if keys do not already exist
#     dependencies: []

include ssh-keygen