# name: IntelliJ IDEA
# description: IDE for Java, and built on Java
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         appdmg
#     dependencies:
#       - jdk
#   Ubuntu:
#     install-method:
#       method:         manual
#       manual-desc:    Installed to $HOME/atlassian/idea12
#     dependencies:
#       - jdk

include idea