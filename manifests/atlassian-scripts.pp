# name: Atlassian Scripts
# description: A collection of useful scripts
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         manual
#       manual-desc:    Installed to $ATLASSIAN_HOME/atlassian-scripts
#     dependencies:
#       - svn
#   Ubuntu:
#     install-method:
#       method:         manual
#       manual-desc:    Installed to $ATLASSIAN_HOME/atlassian-scripts
#     dependencies:
#       - svn

include atlassian-scripts