# name: Maven 3.0.5
# description: For building things based on some rules
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         manual
#       manual-desc:    Installed to $ATLASSIAN_HOME/maven3.0.5
#     dependencies:
#       - jdk
#   Ubuntu:
#     install-method:
#       method:         manual
#       manual-desc:    Installed to $ATLASSIAN_HOME/maven3.0.5
#     dependencies:
#       - jdk

maven {'3.0.5':
	version	=> '3.0.5',
}
