# name: HipChat
# description: Atlassian Chat Client
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         appdmg
#     dependencies: []
#   Ubuntu:
#     install-method:
#       method:         apt
#       apt-repository: http://downloads.hipchat.com/linux/apt
#       apt-package:    hipchat
#     dependencies: []

include hipchat