# name: Python 3
# description: Third major version of Python
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         brew
#       brew-package:   python3
#     dependencies:
#       - homebrew
#   Ubuntu:
#     install-method:
#       method:         apt
#       apt-repository: canonical
#       apt-package:    python3
#     dependencies: []

include python3
