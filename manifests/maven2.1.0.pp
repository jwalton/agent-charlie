# name: Maven 2.1.0
# description: Legacy version of Maven used by most of our product builds
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         manual
#       manual-desc:    Installed to $ATLASSIAN_HOME/maven2.1.0
#     dependencies:
#       - jdk
#   Ubuntu:
#     install-method:
#       method:         manual
#       manual-desc:    Installed to $ATLASSIAN_HOME/maven2.1.0
#     dependencies:
#       - jdk

maven {'2.1.0':
	version	=> '2.1.0',
}
