# name: Maven 'settings.xml' Generation
# description: Generates some settings.xml to allow you to connect to our Maven repos
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         manual
#       manual-desc:    .m2 settings are generated from settings.xml.developer
#     dependencies:
#       - jdk
#       - maven3.0.5
#       - git 
#       - bitbucket-keys
#   Ubuntu:
#     install-method:
#       method:         manual
#       manual-desc:    .m2 settings are generated from settings.xml.developer
#     dependencies:
#       - jdk
#       - maven3.0.5
#       - git
#       - bitbucket-keys

maven::settings {'mavensettings':
	version	=> '3.0.5'
}
