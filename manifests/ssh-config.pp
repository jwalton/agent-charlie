# name: SSH Config
# description: Writes an SSH configuration file
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         manual
#       manual-desc:    Copies over a ~/.ssh/config file
#     dependencies: []
#   Ubuntu:
#     install-method:
#       method:         manual
#       manual-desc:    Copies over a ~/.ssh/config file
#     dependencies: []

include ssh-config