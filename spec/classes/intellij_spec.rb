require 'spec_helper'

describe 'idea' do
    let(:title) { 'intellij_spec' }
    let(:facts) {
        {
            :charlie_repository => 'http://www.test-repository.example.com',
            :config_atlassian_home => '/home/horses/jesus',
            :config_user => 'bob',
            :config_group => 'bobgroup',
            :config_home => '/home/ponies',
            :config_tmp_directory => '/home/ponies/atlassian/agent-charlie/tmp',
            :config_charlie_home => '/home/charlie/directory'
        }
    }

    context 'with operatingsystem => OSX' do
        let(:facts) {{:operatingsystem => 'Darwin'}}
        it {
            should contain_class('idea::intellij::darwin')
            #should contain_idea__intellij__darwin('12.1.1')
        }
    end

    context 'with operatingsystem => Ubuntu' do
        let(:facts) {{
            :operatingsystem        => 'Ubuntu',
            :operatingsystemrelease => '12.04',
            :architecture           => 'x86_64',
        }}
        it {
            should contain_class('idea::intellij::ubuntu')
        }
    end

    context 'with operatingsystem => not_valid_OS' do
        let(:facts) {{:operatingsystem => 'BrickOS_4000'}}
        it {
            expect {
                should contain_fail()
            }.to raise_error(Puppet::Error)
        }
    end
end
