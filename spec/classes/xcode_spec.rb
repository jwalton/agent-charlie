require 'spec_helper'

describe 'xcode' do
    default_facts = {
        :charlie_repository => 'http://www.test-repository.example.com',
        :config_atlassian_home => '/home/ponies/target',
        :config_user => 'bob',
        :config_group => 'bobgroup',
        :config_home => '/home/ponies',
        :config_tmp_directory => '/home/ponies/atlassian/agent-charlie/tmp',
    }

    context 'with unsupported Darwin' do
        let(:facts) {default_facts.merge({
            :operatingsystem        => 'Darwin',
            :macosx_productversion_major => 1337.9000,
        })}
        it {
            expect {
                should contain_fail()
            }.to raise_error(Puppet::Error)
        }
    end

    context 'with Darwin' do
        let(:facts) {default_facts.merge({
            :operatingsystem        => 'Darwin',
            :macosx_productversion_major => 10.8,
        })}
        it {
            should contain_package('xcode_command_line_tools').with({
                'ensure'    =>  'installed',
                'source'    =>  'http://www.test-repository.example.com/xcode_latest_mountainlion.dmg',
            })
        }
    end

    context 'with non-darwin' do
        let(:facts) {default_facts.merge({
            :operatingsystem        => 'Ubuntu',
            :operatingsystemrelease => '11.04',
        })}
        it {
            expect {
                should contain_fail()
            }.to raise_error(Puppet::Error)
        }
    end

end
