require 'spec_helper'

describe 'jdk::darwin' do
    let(:title) { 'jvm_darwin_spec' }
    let(:facts) {
        {
            :operatingsystem => 'Darwin',
            :config_atlassian_home => 'some_target',
            :charlie_repository => 'http://www.test-repository.example.com',
            :config_charlie_home => 'charlie_directory',
            :config_user => 'bob',
            :config_group => 'bobgroup',
        }
    }

    it {
        expect contain_package('jdk').with({
          'ensure' => 'installed',
          'source' => "http://www.test-repository.example.com/jdk_latest.dmg"
        })
    }
end
