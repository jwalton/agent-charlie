#!/usr/bin/ruby
require 'rubygems'
require 'rest-client'
require 'json'

username = ARGV[0]
password = ARGV[1]

# Get keys from BB where label is blah
response = RestClient::Request.execute(
    :method     =>  :get,
    :url        =>  "https://bitbucket.org/api/1.0/users/#{username}/ssh-keys",
    :user       =>  username,
    :password   =>  password
)

parsed = JSON.parse(response)

for key in parsed
    if key['label'] == 'charlie-installed-key'
        RestClient::Request.execute(
            :method     =>  :delete,
            :url        =>  "https://bitbucket.org/api/1.0/users/#{username}/ssh-keys/#{key['pk']}",
            :user       =>  username,
            :password   =>  password
        )
    end
end