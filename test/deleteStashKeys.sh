#!/bin/bash

curl -u "$1:$2" -i -H "Accept: application/json" -X DELETE "https://stash.atlassian.com/rest/ssh/1.0/keys"